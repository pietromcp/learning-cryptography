package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"sort"
	"strings"
)

func main() {

	//content, err := ioutil.ReadFile("italian-constitution.txt")
	//content, err := ioutil.ReadFile("../ciphers/sample/unknown-english-ciphertext.txt")
	content, err := ioutil.ReadFile("../ciphers/sample/unknown-english-vigenere-ciphertext.txt")

	if err != nil {
		log.Fatal(err)
	}
	text := strings.ToLower(string(content))

	//fmt.Println(text)

	var occurrences map[int32]int
	occurrences = make(map[int32]int)

	for _, c := range text {
		if c != ' ' && c != '\r' && c != '\n' {
			occurrences[c] = occurrences[c] + 1
		}
	}

	keys := make([]int32, len(occurrences))

	x := 0
	total :=0
	for k := range occurrences {
		keys[x] = k
		x++
		total += occurrences[k]
	}

	sort.SliceStable(keys, func(i int, j int) bool {
		ix := int32(i)
		jx := int32(j)
		return occurrences[keys[ix]] > occurrences[keys[jx]]
	})

	for _, k := range keys {
		fmt.Printf("%s ==> %4d (%.2f %%)\n", string(k), occurrences[k], 100 * float32(occurrences[k]) / float32(total))
	}
}
