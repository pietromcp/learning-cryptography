# Introduzione alla crittografia
## Argomenti trattati
### Cifrario di Cesare
- implementazione in JS in `ciphers/src/caesar.js`
- script per violarlo in `ciphers/try-to-break-caesar.sh`
### Sostituzione monoalfabetica
- implementazione in CLI mediante `tr`
- implementazione in JS in `ciphers/src/mono.js`
- analisi delle frequenze dei simboli in `frequency-analysis/analyze.go`
- analisi delle frequenze delle parole di tre lettere, alla ricerca di `the`, in `ciphers/src/try-to-break-monoalphabetic.js`
### Cifrario di Vigenere
- implementazione in Ruby in `vigenere/encrypt.rb` e `vigenere/decrypt.rb`
- applicazione dell'analisi delle frequenze dei simboli
- approccio mediante [test di Babbage/Kasiski](https://en.wikipedia.org/wiki/Kasiski_examination)
### Steganografia
- video di gattini; implementazione in C in `steganography/main.c`
- tanti auguri, Jacek; esempio di testo scritto sfruttato come vettore di un messaggio nascosto, in `Auguri.md`
