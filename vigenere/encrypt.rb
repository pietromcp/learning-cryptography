ZERO = "a".ord
RANGE = "z".ord - ZERO + 1

def encrypt(key, input)
  k = key.downcase.split("").filter do |c|
    c.match?(/[a-z]/)
  end

  output = input.downcase.split("").each_with_index.map do |c, i|
    encrypted = c.match?(/[a-z]/) ? ((c.ord - ZERO + k[i % k.length].ord - ZERO) % RANGE + ZERO).chr : c
    puts "Char #{c} --> #{encrypted}"
    encrypted
  end
  output.join("")
end

key = File.open("key.txt")
        .readlines
          .join(" ")

# input = File.open("infinito.txt")
input = File.open("../ciphers/sample/is-design-dead-cleartext.txt")
            .readlines
            .join("")

output = encrypt(key, input)

# File.open("encrypted.txt", "w").write(output)
File.open("../ciphers/sample/unknown-english-vigenere-ciphertext.txt", "w").write(output)
