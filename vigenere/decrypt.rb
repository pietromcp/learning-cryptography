ZERO = "a".ord
RANGE = "z".ord - ZERO + 1

def decrypt(key, input)
  k = key.downcase.split("").filter do |c|
    c.match?(/[a-z]/)
  end

  output = input.downcase.split("").each_with_index.map do |c, i|
    decrypted = c.match?(/[a-z]/) ? ((c.ord - ZERO + RANGE - (k[i % k.length].ord - ZERO)) % RANGE + ZERO).chr : c
    decrypted
  end
  output.join("")
end

key = File.open("key.txt")
        .readlines
          .join(" ")

input = File.open("encrypted.txt")
            .readlines
            .join("")

output = decrypt(key, input)

puts output
