#include <stdio.h>
#include <string.h>

void encode(int key) {
    const char *inputFileName = "/home/pietro/sources/learning/learning-cryptography/steganography/original-video.mp4";
    const char *outputFileName = "/home/pietro/sources/learning/learning-cryptography/steganography/encoded-video.mp4";
    char secret[1000] = "Gli arcobaleni d'altri mondi hanno colori che non so";
    const char * messageEnd = "#";
    strcat(secret, messageEnd);

    FILE *input, *output;
    const int bufferSize = 1024;

    input = fopen(inputFileName,"rb");
    output = fopen(outputFileName,"wb");
    char buffer[bufferSize];
    int page = 0, min, max;
    int secretItem = 0;

    while(fread(buffer,sizeof(buffer),1,input) > 0) {
        if(secretItem < strlen(secret)) {
            min = page * bufferSize;
            max = (page + 1) * bufferSize;
            int nextItemIndex = key * (secretItem + 1);
            if(min <= nextItemIndex && nextItemIndex < max) {
                buffer[nextItemIndex - min] = secret[secretItem];
                printf("Encoded secret item %c for index%d\n", buffer[nextItemIndex - min], nextItemIndex);
                secretItem++;
            }
        }
        fwrite(buffer,sizeof(buffer),1,output);
        page++;
    }

    fclose(input);
    fclose(output);

    printf("Files closed.\n");
}

void decode(int key) {
    printf("Opening files...\n");
    const char *inputFileName = "/home/pietro/sources/learning/learning-cryptography/steganography/encoded-video.mp4";
    char messageEnd = '#';
    FILE *input;
    const int bufferSize = 1024;

    input = fopen(inputFileName, "rb");
    printf("Start reading...\n");
    unsigned char b = 1;
    char buffer[bufferSize];
    int page = 0, min = 0, max = 0;
    int secretItem = 0;
    int endReached = 0;
    char message[1000] = "";
    while (!endReached && fread(buffer, sizeof(buffer), 1, input) > 0) {
        min = page * bufferSize;
        max = (page + 1) * bufferSize;
        int nextItemIndex = key * (secretItem + 1);
        if (min <= nextItemIndex && nextItemIndex < max) {
            char item = buffer[nextItemIndex - min];
            if(messageEnd - item == 0) {
                endReached = 1;
            } else {
                strncat(message, &item, 1);
            }
            secretItem++;
        }
        page++;
    }
    printf("Decoded secret: %s", message);
    fclose(input);
}

int main(int argc, char *argv[]) {
    const int key = 15000;
    char mode = 'd';
    if(argc == 2) {
        mode = argv[1][0];
    }
    printf("Mode is %c\n", mode);
    switch (mode) {
        case 'e': {
            encode(key);
            break;
        }

        case 'd': {
            decode(key);
            break;
        }

        default: {
            fprintf(stderr, "Error: unknown mode %c", mode);
            return 1;
        }
    }
    return 0;
}
