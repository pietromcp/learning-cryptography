*Simboli*: `0, 1, 2, 3`

*Encoding simboli*: `00, 01, 10, 11`

*Messaggio*: `0123210`
*Encoding messaggio*: `00011011100100`

*Check encoding*:     `00011011100100`

```
Caro Jacek,
oggi è il giorno in cui tu e la Regina d'Inghilterra celebrate la ricorrenza del
vostro compleanno.
Che la festa e la vostra giornata siano buone!
Vi giungano le mie felicitazioni per questo evento ed il mio augurio più sincero di
lunga vita - più probabile nel tuo caso che nel suo.

Pietro.
```
