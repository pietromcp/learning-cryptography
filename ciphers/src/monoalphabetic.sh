#!/bin/bash
#
#
#
if [ $# -ne 2 ]
then
  >&2 echo "Usage: monoalphabetic.sh mode key"
  exit 1
fi

mode=$1
key=$2

>&2 echo "Doing monoalphabetic substitution. Key = ${key} mode = ${mode}"

case "${mode}" in
  "e" | "E" )
    >&2 echo "Encryption"
    tr 'abcdefghijklmnopqrstuvwxyz' $key
  ;;
  "d" | "D" )
    >&2 echo "Decryption"
    tr $key 'abcdefghijklmnopqrstuvwxyz'
  ;;  
esac



# qwertyuiopasdfghjklzxcvbnm
# abcdefghijklmnopqrstuvwxyz
