function range(first, last) {
    const length = last - first + 1
    return new Array(length).fill(-1, 0, length).map(function(v, i) { return i + first; })
}

function substringsOf(text, length) {
    return text.length < length ? [] : range(0, text.length - length).map(i => text.substring(i, i + length))
}

function countOccurrences(words) {
    return words.reduce((ws, w) => Object.assign({}, ws, {[w]: (ws[w] || 0) + 1}), {})
}

function sortKeysByValue(object, mode = 'asc') {
    const keys = Object.keys(object)
    const comparer = mode === 'asc'
        ? (a, b) => a - b
        : (a, b) => b - a
    const sortedKeys = keys.sort((a, b) => comparer(object[a], object[b]))
    return sortedKeys.map(k => ({
        key: k,
        count: object[k]
    }))
}

module.exports = function breaker(ciphertext) {
    const by3 = substringsOf(ciphertext, 3)
    const occurrences = countOccurrences(by3)
    const sorted = sortKeysByValue(occurrences, 'desc')
    return sorted.slice(0, 5)
}