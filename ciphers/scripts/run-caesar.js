const caesar = require('../src/caesar')

// console.error('argv', process.argv.map((x, i) => `${i} ${x}`))

if(process.argv.length !== 3) {
    console.error("Usage: npm run caesar key < input_file")
    console.error("  1 <= key <= 25")
    process.exit(1)
}

const key = parseInt(process.argv[2])
console.error('Using key', key)

const readline = require('linebyline')
const rl = readline(process.stdin)
  rl.on('line', function(line, lineCount, byteCount) {
    console.log(caesar(line, key))
  })
  .on('error', function(err) {
    console.log(`??? ${err}`)
  });