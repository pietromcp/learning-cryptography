const { monoalphabetic: mono, operations } = require('../src/monoalphabetic')

function showUsageAndExit() {
  console.error("Usage npm run mono encrypt|decrypt key_file < input_file")
  process.exit(1)
}

if(process.argv.length !== 4) {
    showUsageAndExit()
}

const mode = process.argv[2]

if(!operations.includes(mode)) {
  showUsageAndExit()
}

console.error('Using mode', mode)
const keyFile = process.argv[3]

const fs = require('fs')
const readline = require('linebyline')

const keyRows = []
const keyReader = readline(fs.createReadStream(keyFile))
keyReader.on('line', function(line, lineCount, byteCount) {
    keyRows.push(line)
  }).on('error', function(err) {
    console.error(`??? ${err}`)
  }).on('close', function(err) {
    const key = keyRows.join('')
    console.error('Using key', key)
    const rl = readline(process.stdin)
    rl.on('line', function(line, lineCount, byteCount) {
      console.log(mono(line, mode, key))
    }).on('error', function(err) {
      console.log(`??? ${err}`)
    });
  })
