const breaker = require('../src/try-to-break-monoalphabetic')

// console.error('argv', process.argv.map((x, i) => `${i} ${x}`))

if(process.argv.length !== 2) {
    console.error("Usage: npm run mono-breaker < input_file")
    process.exit(1)
}

const fs = require('fs')

fs.readFile(0, 'utf8', function(err, data) {
    if (err) throw err
    console.log('Before', new Date())
    const found = breaker(data)
    console.log('After', new Date())
    console.log('Most frequent groups of three chars are', found.map(x => `${x.key} --> ${x.count}`))
});